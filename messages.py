from tkinter import messagebox


messages = {
    "play" : "Play",
    "exit" : "Exit",
    "win" : "Congratulations! The Winner is: ",
    "tie" : "It's a tie",
    "again" : "Do you want to play again?",
    "ask_exit": "Do you want to exit?",
    "bye": "Goodbye"
    }


def say_message(title, msj):  # Muestra un mensaje informativo
    messagebox.showinfo(title, msj)
    return 0


def ask_message(title, msj):  # pregunta si desea realizar una accion especifica por medio de ventanas
    question = messagebox.askokcancel(title, msj)
    return question
