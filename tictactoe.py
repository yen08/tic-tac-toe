from tkinter import *
from messages import *


def mostrar_ventana_inicio():
    global c
    ventana = Tk()
    ventana.title("tictactoe")
    ventana.minsize(600, 600)
    ventana.resizable(width=False, height=False)

    c = Canvas(ventana, width=600, height=600, bg="#D89A1F")
    c.pack()

    c.create_line(0, 200, 600, 200, width=9)
    c.create_line(0, 400, 600, 400, width=9)
    c.create_line(200, 0, 200, 600, width=9)
    c.create_line(400, 0, 400, 600, width=9)

    c.bind("<Button-1>", click)

    ventana.mainloop()


shape = "O"

grid = ["0", "1", "2",
        "3", "4", "5",
        "6", "7", "8"]

contador = 0


def click(event):
    global c, shape, grid, across, down, contador
    across = int(c.canvasx(event.x) / 200)
    down = int(c.canvasy(event.y) / 200)

    square = across + (down * 3)

    if grid[square] == "X" or grid[square] == "O":
        return

    if shape == "O":
        c.create_oval(across * 200, down * 200, (across + 1) * 200, (down + 1) * 200, width=9)
        grid[square] = "O"
        shape = "X"
    else:
        c.create_line(across * 200, down * 200, (across + 1) * 200, (down + 1) * 200, width=9)
        c.create_line(across * 200, (down + 1) * 200, (across + 1) * 200, down * 200, width=9)
        grid[square] = "X"
        shape = "O"
    contador += 1
    verificar_ganador()


def verificar_ganador():
    global grid

    Fila1 = grid[0:3]
    Fila2 = grid[3:6]
    Fila3 = grid[6:9]

    Matriz = [Fila1, Fila2, Fila3]

    if contador <= 8:
        if shape == "O":

            if Matriz[0][0] == "X" and Matriz[1][0] == "X" and Matriz[2][0] == "X":
                Win("X")
            elif Matriz[0][1] == "X" and Matriz[1][1] == "X" and Matriz[2][1] == "X":
                Win("X")
            elif Matriz[0][2] == "X" and Matriz[1][2] == "X" and Matriz[2][2] == "X":
                Win("X")
            elif Matriz[0][0] == "X" and Matriz[0][1] == "X" and Matriz[0][2] == "X":
                Win("X")
            elif Matriz[1][0] == "X" and Matriz[1][1] == "X" and Matriz[1][2] == "X":
                Win("X")
            elif Matriz[2][0] == "X" and Matriz[2][1] == "X" and Matriz[2][2] == "X":
                Win("X")
            elif Matriz[0][0] == "X" and Matriz[1][1] == "X" and Matriz[2][2] == "X":
                Win("X")
            elif Matriz[0][2] == "X" and Matriz[1][1] == "X" and Matriz[2][0] == "X":
                Win("X")

        if shape == "X":
            if Matriz[0][0] == "O" and Matriz[1][0] == "O" and Matriz[2][0] == "O":
                Win("O")
            elif Matriz[0][1] == "O" and Matriz[1][1] == "O" and Matriz[2][1] == "O":
                Win("O")
            elif Matriz[0][2] == "O" and Matriz[1][2] == "O" and Matriz[2][2] == "O":
                Win("O")
            elif Matriz[0][0] == "O" and Matriz[0][1] == "O" and Matriz[0][2] == "O":
                Win("O")
            elif Matriz[1][0] == "O" and Matriz[1][1] == "O" and Matriz[1][2] == "O":
                Win("O")
            elif Matriz[2][0] == "O" and Matriz[2][1] == "O" and Matriz[2][2] == "O":
                Win("O")
            elif Matriz[0][0] == "O" and Matriz[1][1] == "O" and Matriz[2][2] == "O":
                Win("O")
            elif Matriz[0][2] == "O" and Matriz[1][1] == "O" and Matriz[2][0] == "O":
                Win("O")
    else:
        say_message("Tic Tac Toe", "Ningún jugador ganó")
        exit(0)


def Win(player):
    say_message("Tic Tac Toe", "El jugador " + player + " ha ganado")
    exit(0)


mostrar_ventana_inicio()
